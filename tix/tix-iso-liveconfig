#!/bin/sh
# Copyright (c) 2017 Jonas 'Sortie' Termansen.
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# tix-iso-liveconfig
# Generate configuration files for customizing the live environment.

set -e

directory=
hostname=
kblayout=
operand=1
videomode=

dashdash=
previous_option=
for argument do
  if test -n "$previous_option"; then
    eval $previous_option=\$argument
    previous_option=
    continue
  fi

  case $argument in
  *=?*) parameter=$(expr "X$argument" : '[^=]*=\(.*\)' || true) ;;
  *=)   parameter= ;;
  *)    parameter=yes ;;
  esac

  case $dashdash$argument in
  --) dashdash=yes ;;
  --hostname=*) hostname=$parameter ;;
  --hostname) previous_option=hostname ;;
  --kblayout=*) kblayout=$parameter ;;
  --kblayout) previous_option=kblayout ;;
  --videomode=*) videomode=$parameter ;;
  --videomode) previous_option=videomode ;;
  -*) echo "$0: unrecognized option $argument" >&2
      exit 1 ;;
  *)
    if [ $operand = 1 ]; then
      directory="$argument"
      operand=2
    else
      echo "$0: unexpected extra operand $argument" >&2
      exit 1
    fi
    ;;
  esac
done

if test -n "$previous_option"; then
  echo "$0: option '$argument' requires an argument" >&2
  exit 1
fi

if test -z "$directory"; then
  echo "$0: No directory was specified" >&2
  exit 1
fi

mkdir -p "$directory"

if [ -n "$hostname" ]; then
  mkdir -p -- "$directory/etc"
  printf "%s\n" "$hostname" > "$directory/etc/hostname"
else
  hostname=sortix
fi

if [ -n "$kblayout" ]; then
  printf "%s\n" "$kblayout" > "$directory/etc/kblayout"
fi

if [ -n "$videomode" ]; then
  printf "%s\n" "$videomode" > "$directory/etc/videomode"
fi
